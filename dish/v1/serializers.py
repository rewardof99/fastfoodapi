from rest_framework import serializers

from dish.models import Dish
from file.v1.serializers import FileSerializer


class DishSerializer(serializers.ModelSerializer):
    actual_price = serializers.SerializerMethodField()

    class Meta:
        model = Dish
        fields = (
            'id',
            'name',
            'description',
            'price',
            'actual_price',
            'image',
            'discount',
            'is_active',
            'created_at',
            'modified_at',
        )
        read_only_fields = (
            'id',
            'created_at',
            'modified_at',
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['image'] = FileSerializer(instance.image).data if instance.image else None

        return representation

    def validate(self, attrs):
        discount = attrs.get('discount')
        price = attrs.get('price')
        if discount and (discount < 0 or discount > 100):
            raise serializers.ValidationError("Discount must be between 0 and 100")

        if price and price <= 0:
            raise serializers.ValidationError("Price must be greater than 0")

        return attrs

    def get_actual_price(self, obj):
        return obj.actual_price
