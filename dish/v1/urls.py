from django.urls import include, path
from rest_framework.routers import DefaultRouter

from dish.v1.views import (
    DishViewSet
)

router = DefaultRouter()

router.register(r'', DishViewSet, basename='dishs')

urlpatterns = [
    path('', include(router.urls))
]
