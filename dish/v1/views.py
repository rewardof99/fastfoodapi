from rest_framework.permissions import IsAuthenticated

from base.views import BaseModelViewSet
from dish.models import Dish
from dish.v1.serializers import DishSerializer
from user.permissions import (
    IsStaffUser
)


class DishViewSet(BaseModelViewSet):
    queryset = Dish.objects.select_related(
        "image"
    ).all().order_by('name')
    serializer_class = DishSerializer
    permission_classes = (IsAuthenticated,)
    ordering_fields = (
        'id',
        'name',
        'price',
        'discount',
        'created_at'
    )
    filterset_fields = (
        'is_active',
    )
    search_fields = (
        'name',
        'description'
    )

    def get_permissions(self):
        if self.action in ('list', 'retrieve'):
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsStaffUser]

        return [permission() for permission in permission_classes]
