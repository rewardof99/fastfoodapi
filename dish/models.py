from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import NotAcceptable

from base.models import BaseModel
from utils.validators import phone_number_validator


class Restaurant(models.Model):
    """
    only one restaurant can be created
    """
    name = models.CharField(
        _("name"),
        max_length=255
    )
    address = models.TextField(_("Restaurant address"))
    longitude = models.FloatField(_("longitude of restaurant location"))
    latitude = models.FloatField(_("latitude of restaurant location"))
    contact_number = models.CharField(
        _('phone number'),
        validators=[phone_number_validator()],
        max_length=17,
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.pk is None and Restaurant.objects.exists():
            raise NotAcceptable(_("Only one restaurant can be created"))
        super().save(*args, **kwargs)


class Dish(BaseModel):
    name = models.CharField(
        _("Dish name"),
        max_length=255
    )
    description = models.TextField(_("Dish description"))
    price = models.DecimalField(
        _("Dish price"),
        max_digits=10, decimal_places=2
    )
    image = models.ForeignKey(
        to='file.File',
        verbose_name=_("Dish image"),
        on_delete=models.SET_NULL,
        null=True
    )
    # 0-100
    discount = models.DecimalField(
        _("Dish discount"),
        max_digits=4, decimal_places=2,
        null=True
    )
    is_active = models.BooleanField(_("Is dish active"), default=True)

    def __str__(self):
        return self.name

    @property
    def actual_price(self):
        discount = self.discount
        price = self.price
        if discount:
            return price - (price * discount / 100)
        return price
