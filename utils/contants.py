from enum import Enum

from django.utils.translation import gettext_lazy as _


class Status(Enum):
    DRAFT = 'Draft'
    PUBLISHED = 'Published'
    ARCHIVED = 'Archived'


class Languages:
    ENGLISH = 'English'
    UZBEK = 'uzbek'
    RUSSIAN = 'russian'

    CHOICES = (
        (ENGLISH, _('English')),
        (UZBEK, _('Uzbek')),
        (RUSSIAN, _('Russian')),
    )


class UserTypes:
    CUSTOMER = 'Customer'
    WAITER = 'Waiter'
    ADMIN = 'Admin'

    CHOICES = (
        (CUSTOMER, _('Customer')),
        (ADMIN, _('Admin')),
        (WAITER, _('Waiter')),
    )
    LIST = [CUSTOMER, ADMIN, WAITER]

    STAFFS = (ADMIN, WAITER)


class Gender:
    MALE = 'male'
    FEMALE = 'female'
    CHOICES = (
        (MALE, _("Male")),
        (FEMALE, _("Female"))
    )


class OrderStatus:
    CANCELLED = 'Cancelled'
    RECEIVED = 'Received'
    PREPARING = 'Preparing'
    ON_DELIVERY = 'On Delivery'
    DELIVERED = 'Delivered'

    CHOICES = (
        (CANCELLED, _("Cancelled")),
        (RECEIVED, _("Received")),
        (PREPARING, _("Preparing")),
        (ON_DELIVERY, _("On Delivery")),
        (DELIVERED, _("Delivered")),
    )


class PaymentMethod:
    VISA = 'visa'
    MASTERCARD = 'mastercard'
    PAYPAL = 'paypal'
    STRIPE = 'stripe'
    WALLET = 'wallet'
    CASH = 'cash'
    DEFAULT = STRIPE

    CHOICES = (
        (VISA, _(VISA)),
        (MASTERCARD, _(VISA)),
        (PAYPAL, _(PAYPAL)),
        (STRIPE, _(STRIPE)),
        (WALLET, _(WALLET)),
        (CASH, _(CASH))
    )
