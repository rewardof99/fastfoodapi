from django.urls import path, include
from rest_framework import routers

from order.v1.views import (
    UserCartAPIView,
    AddToCartAPIView,
    RemoveFromCartAPIView,
    UserOrderViewSet,
    OrderViewSet
)

app_name = 'order'

router = routers.DefaultRouter()
router.register('user-orders', UserOrderViewSet, 'user-orders')
router.register('orders', OrderViewSet, 'orders')

urlpatterns = [
    path('user-cart/', UserCartAPIView.as_view(), name='user-cart'),
    path('add-to-cart/', AddToCartAPIView.as_view(), name='add-to-cart'),
    path('remove-from-cart/', RemoveFromCartAPIView.as_view(), name='remove-from-cart'),
    path('', include(router.urls)),
]
