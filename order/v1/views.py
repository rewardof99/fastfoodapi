from rest_framework import generics
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated

from base.views import BaseModelViewSet
from order.models import UserCart, Order
from order.services import UserCartManager
from order.v1.serializers import (
    UserCartSerializer,
    CartUpdateSerializer, UserOrderSerializer, OrderSerializer
)
from user.permissions import IsStaffUser
from utils.response import SuccessResponse


class UserCartAPIView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        """
        Get the cart items of the user
        """
        cart_items = self.get_queryset()
        serializer = UserCartSerializer(cart_items, many=True)
        data = {
            "data": {
                "cart_items": serializer.data,
                "total_price": sum([item.total_price for item in cart_items])
            }
        }
        return SuccessResponse(**data)

    def get_queryset(self):
        user = self.request.user
        return UserCart.objects.select_related(
            'dish', 'dish__image', 'user'
        ).filter(user=user).order_by('-id')


class AddToCartAPIView(generics.CreateAPIView):
    serializer_class = UserCartSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """
        Add a dish to the cart
        """
        serializer = CartUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        cart_manager = UserCartManager(request.user)
        cart_manager.add_dish(**serializer.validated_data)
        return SuccessResponse()


class RemoveFromCartAPIView(generics.DestroyAPIView):
    permission_classes = (IsAuthenticated,)

    def destroy(self, request, *args, **kwargs):
        """
        Remove a dish from the cart
        """
        cart_manager = UserCartManager(request.user)
        cart_manager.remove_dish(dish_id=self.request.data.get('dish_id'))
        return SuccessResponse()


class UserOrderViewSet(BaseModelViewSet):
    serializer_class = UserOrderSerializer
    filtersearch_fields = ('status',)

    def get_queryset(self):
        return Order.objects.select_related(
            "user",
        ).prefetch_related(
            'dishes'
        ).filter(user=self.request.user).order_by('-id')

    @action(detail=True, methods=['PATCH'])
    def cancel_order(self, request, *args, **kwargs):
        """
        Cancel an order
        """
        order = self.get_object()
        order.cancel_order(
            cancellation_reason=request.data.get('cancellation_reason')
        )
        return SuccessResponse()


class OrderViewSet(BaseModelViewSet):
    """
    ViewSet for manage orders in admin panel
    only staff users(admin, waiter) can do this actions
    """
    serializer_class = OrderSerializer
    filtersearch_fields = ('status', 'user')
    search_fields = ('user__full_name',)
    ordering_fields = ('id', 'status', 'total_amount', 'created_at')
    permission_classes = (IsStaffUser,)
    http_method_names = ('get', 'patch', 'put')

    def get_queryset(self):
        return Order.objects.select_related(
            "user",
        ).prefetch_related(
            'dishes'
        ).order_by('-id')

    def partial_update(self, request, *args, **kwargs):
        """
        Update an order
        only status, cancellation_reason can be updated
        """
        return super().partial_update(request, *args, **kwargs)
