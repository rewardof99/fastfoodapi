from rest_framework import serializers

from dish.v1.serializers import DishSerializer
from order.models import UserCart, Order
from order.services import UserOrderManager
from user.v1.serializers import UserMiniSerializer


class UserCartSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
        write_only=True
    )

    class Meta:
        model = UserCart
        fields = (
            'dish',
            'user',
            'quantity',
            'total_price'
        )

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['dish'] = DishSerializer(instance.dish).data if instance.dish else None
        return data


class CartUpdateSerializer(serializers.Serializer):
    dish_id = serializers.IntegerField(required=True, allow_null=False)
    quantity = serializers.IntegerField(
        required=True, allow_null=False,
        min_value=1
    )


class UserOrderSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
        write_only=True
    )

    class Meta:
        model = Order
        fields = (
            'id',
            'user',
            'dishes',
            'estimated_delivery_time',
            'status',
            'total_amount',
            'payment_method',
            'cancellation_reason',
            'history'
        )
        read_only_fields = (
            'id',
            'estimated_delivery_time',
            'dishes',
            'payment_method',
            'history',
            'total_amount',
        )

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['dishes'] = (
            DishSerializer(instance.dishes, many=True).data
            if instance.dishes else None
        )
        return data

    def create(self, validated_data):
        order = UserOrderManager(
            user=self.context['request'].user
        ).create_order(
            payment_method=validated_data.get('payment_method')
        )
        return order


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = (
            'id',
            'user',
            'dishes',
            'estimated_delivery_time',
            'status',
            'total_amount',
            'payment_method',
            'cancellation_reason',
            'history'
        )
        read_only_fields = (
            'id',
            'user',
            'estimated_delivery_time',
            'dishes',
            'payment_method',
            'history',
            'total_amount',
        )

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['dishes'] = (
            DishSerializer(instance.dishes, many=True).data
            if instance.dishes else None
        )
        data['user'] = UserMiniSerializer(instance.user).data
        return data
