from django.db import transaction
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404

from dish.models import Dish, Restaurant
from order.models import UserCart, Order
from utils.contants import OrderStatus


class UserCartManager:
    def __init__(self, user):
        self.user = user

    def add_dish(self, dish_id, quantity=1):
        # type: (int, int) -> None
        """
        Add a dish to the cart or update the quantity if it already exists
        """
        # only active dishes can be added to cart
        dish = get_object_or_404(Dish, id=dish_id, is_active=True)
        cart_item, created = UserCart.objects.get_or_create(
            user=self.user, dish=dish,
            defaults={'quantity': quantity}
        )
        if not created:
            cart_item.quantity = int(quantity)
            cart_item.save(update_fields=['quantity'])

    def remove_dish(self, dish_id):
        # type: (int) -> None
        """
        Remove a dish from the cart
        """
        dish = get_object_or_404(Dish, id=dish_id)
        cart_item = get_object_or_404(UserCart, user=self.user, dish=dish)
        cart_item.delete()


class UserOrderManager:
    def __init__(self, user):
        self.user = user
        self.user_cart = None
        self.number_of_dishes = None
        self.dishes = None

    def create_order(
            self,
            payment_method,
    ):
        # type: (str) -> 'Order'
        self.validate()

        self.set_attributes()

        with transaction.atomic():
            order = Order.objects.create(
                user=self.user,
                total_amount=self.calculate_total_amount(),
                estimated_delivery_time=self.calculate_estimated_delivery_time(),
                payment_method=payment_method,
                history=self.build_history()
            )
            order.dishes.set(self.dishes)

            # clear the cart after the order is placed
            self.user.clear_cart()

        return order

    def validate(self):
        user_cart = UserCart.objects.filter(user=self.user)
        if user_cart.count() == 0:
            raise ValidationError(
                _("Cart is empty")
            )

        self.user_cart = user_cart

    def set_attributes(self):
        self.dishes = [item.dish for item in self.user_cart]
        self.number_of_dishes = len(self.dishes)

    def build_history(self):
        history = []
        for item in self.user_cart:
            history.append({
                'dish': item.dish.id,
                'quantity': item.quantity,
                'total_price': float(item.total_price * item.quantity),  # quantity * price
                'price': float(item.dish.price),  # one dish price
                'actual_price': float(item.dish.actual_price),  # one dish actual price(discount)
            })
        return history

    def calculate_total_amount(self):
        total_amount = 0
        for item in self.user_cart:
            total_amount += item.dish.actual_price * item.quantity
        return total_amount

    def calculate_estimated_delivery_time(self):
        distance = self._calculate_distance()
        preparing_dishes = self._get_preparing_dishes_count()
        dishes_per_interval = 4  # Number of dishes prepared in each 5-minute interval
        preparation_interval = 5  # Time in minutes for preparing dishes
        delivery_time_per_km = 3  # Time in minutes per kilometer for delivery

        preparation_time = ((self.number_of_dishes + preparing_dishes) // dishes_per_interval * preparation_interval)
        delivery_time = distance * delivery_time_per_km

        return preparation_time + delivery_time

    def _calculate_distance(self):
        restaurant = Restaurant.objects.first()
        if not restaurant:
            raise ValidationError(
                _("Restaurant not found")
            )

        if not self.user.latitude or not self.user.longitude:
            raise ValidationError(
                _("User location not found")
            )
        # here we will use google maps api or other service to calculate
        # the distance between the user and the restaurant
        # I skipped this part because it is not related to the task
        return 0

    def _get_preparing_dishes_count(self):
        """
        Returns the number of dishes that are being prepared
        """
        return Order.objects.filter(
            status=OrderStatus.PREPARING
        ).count()
