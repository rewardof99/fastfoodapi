from django.db import models
from django.utils.translation import gettext_lazy as _

from base.models import BaseModel
from utils.contants import OrderStatus, PaymentMethod


class Order(BaseModel):
    user = models.ForeignKey(
        'user.User', on_delete=models.CASCADE
    )
    dishes = models.ManyToManyField('dish.Dish')
    estimated_delivery_time = models.IntegerField(null=True)  # in minutes
    status = models.CharField(
        max_length=20,
        choices=OrderStatus.CHOICES,
        default=OrderStatus.RECEIVED
    )
    total_amount = models.DecimalField(
        _("total amount"),
        max_digits=10, decimal_places=2
    )
    payment_method = models.CharField(
        max_length=20,
        choices=PaymentMethod.CHOICES,
        null=True
    )
    cancellation_reason = models.TextField(
        _("cancellation reason"),
        null=True
    )
    # to store dishes and their other details
    history = models.JSONField(
        _("history"),
        default=dict
    )

    def __str__(self):
        return f"{self.user.full_name} - {self.total_amount}"

    def cancel_order(self, cancellation_reason):
        # type: (str) -> None
        """
        Cancel the order
        """
        if self.status != OrderStatus.RECEIVED:
            raise ValueError(_("Order cannot be cancelled"))

        self.status = OrderStatus.CANCELLED
        self.cancellation_reason = cancellation_reason
        self.save(update_fields=['status', 'cancellation_reason'])


class UserCart(BaseModel):
    user = models.ForeignKey(
        'user.User',
        on_delete=models.CASCADE
    )
    dish = models.ForeignKey(
        'dish.Dish',
        verbose_name=_("Dish"),
        on_delete=models.CASCADE
    )
    quantity = models.IntegerField()

    def __str__(self):
        return f"{self.user.full_name} - {self.dish.name} - {self.quantity}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['user', 'dish'],
                name='unique_cart'
            )
        ]

    @property
    def total_price(self):
        return self.dish.actual_price * self.quantity
