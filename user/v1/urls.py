from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from user.v1.views import (
    UserSigninAPIView,
    UserProfileAPIView,
    ConfirmPhoneAPIView,
    CreateStaffAPIView, CustomTokenObtainPairView
)

urlpatterns = [
    path('signin/', UserSigninAPIView.as_view(), name='signin'),
    path('confirm-phone/', ConfirmPhoneAPIView.as_view(), name='confirm-phone'),
    path('me/', UserProfileAPIView.as_view(), name='me'),

    path('create-sfatt/', CreateStaffAPIView.as_view(), name='create-staff'),

    path('token/', CustomTokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
]
