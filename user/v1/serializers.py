from rest_framework import serializers

from user.managers import UserUniqueIdentifierChecker
from user.models import User
from utils.validators import phone_number_validator


class UserSigninSerializer(serializers.Serializer):
    phone_number = serializers.CharField(
        required=True,
        allow_null=False,
        validators=(phone_number_validator(),)
    )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'full_name',
            'phone_number',
            'language',
            'last_login',
            'address',
            'longitude',
            'latitude',
        )


class StaffUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'full_name',
            'phone_number',
            'date_of_birth',
            'gender',
            'language',
            'last_login',
            'user_type',
            'password',
        )
        extra_kwargs = {
            'full_name': {'required': True, 'allow_null': False},
            'gender': {'required': True, 'allow_null': False},
            'date_of_birth': {'required': True, 'allow_null': False},
            'user_type': {'required': True, 'allow_null': False},
            'password': {'required': True, 'allow_null': False, 'write_only': True},
        }

    def validate(self, attrs):
        if attrs['user_type'] == 'CUSTOMER':
            raise serializers.ValidationError('You cannot create customer user')
        checker = UserUniqueIdentifierChecker(attrs['phone_number'])
        if checker.is_registered():
            raise serializers.ValidationError('User with this phone number already exist')
        return attrs

    def create(self, validated_data):
        user = User.objects.create_staff_user(**validated_data)
        return user


class ConfirmAccountSerializer(serializers.Serializer):
    phone_number = serializers.CharField(
        required=True,
        allow_null=False,
        validators=[phone_number_validator(), ]
    )
    confirmation_code = serializers.CharField(required=True, allow_null=False)


class UserMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'full_name',
            'phone_number',
        )