from django.utils.translation import gettext_lazy as _
from rest_framework import generics, views
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from user.managers import (
    UserAuthManager,
    UserUniqueIdentifierChecker
)
from user.models import User
from user.permissions import (
    IsSuperUser,
    IsAuthenticatedUserOrAdmin,
    IsCustomer
)
from user.v1.serializers import (
    UserSigninSerializer,
    UserSerializer,
    ConfirmAccountSerializer,
    StaffUserSerializer
)
from utils.contants import UserTypes
from utils.response import SuccessResponse, FailResponse


class UserSigninAPIView(generics.CreateAPIView):
    serializer_class = UserSigninSerializer

    def get_queryset(self):
        return User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_auth_manager = UserAuthManager(serializer.validated_data.get('phone_number'))
        # if user is new, after confirmation with code
        # full_name must be asked in the next step
        user, is_new_user = user_auth_manager.get_user()
        data = {
            "message": _("Confirmation code has been sent to your phone number"),
            "data": {
                "user": UserSerializer(user).data,
                "is_new_user": is_new_user
            }
        }
        return SuccessResponse(**data)


class UserProfileAPIView(generics.RetrieveAPIView, generics.UpdateAPIView):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticatedUserOrAdmin,)

    def get_object(self):
        return self.request.user

    def partial_update(self, request, *args, **kwargs):
        """
        only full_name, address, longitude, latitude, language can be updated
        """
        user = self.get_object()
        user_serializer = self.get_serializer(user, data=request.data, partial=True)
        user_serializer.is_valid(raise_exception=True)
        user_serializer.save()
        return SuccessResponse(**{"data": user_serializer.data})


class CustomTokenObtainPairView(TokenObtainPairView):

    def get_serializer_class(self):
        return TokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        """
        only admin and waiter can login with password
        customers can login with confirmation code
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if serializer.user.user_type not in UserTypes.STAFFS:
            kwargs = {
                "code": "not_allowed",
                "message": _("You are not allowed to login"),
                'status': 403,
            }
            return FailResponse(**kwargs)

        serializer.is_valid(raise_exception=True)
        data = {
            "message": _("User successfully logged in"),
            "data": {
                "token": serializer.validated_data,
                "user": StaffUserSerializer(serializer.user).data
            }
        }
        return SuccessResponse(**data)


class ConfirmPhoneAPIView(views.APIView):
    """
    For confirming phone number
    """
    permission_classes = (IsCustomer,)

    def post(self, request, *args, **kwargs):
        serializer = ConfirmAccountSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        checker = UserUniqueIdentifierChecker(
            phone_number=serializer.validated_data.get('phone_number')
        )
        if checker.is_not_registered_yet():
            kwargs = {
                "code": "not_found",
                "message": _("User with this email or phone does not exist"),
                'status': 404,
            }
            return FailResponse(**kwargs)

        checker.user.check_confirmation_code(
            confirmation_code=serializer.validated_data.get('confirmation_code')
        )

        data = {
            "message": _("Your account has been confirmed successfully"),
            "data": {
                "token": checker.user.get_pair_token(),
                "user": UserSerializer(checker.user).data
            }
        }
        return SuccessResponse(**data)


class CreateStaffAPIView(generics.CreateAPIView):
    serializer_class = StaffUserSerializer
    permission_classes = (IsSuperUser,)

    def get_queryset(self):
        return User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        data = {
            "message": _("Staff has been created successfully"),
            "data": {
                'token': user.get_pair_token(),
                "user": self.get_serializer(user).data,
            }
        }
        return SuccessResponse(**data)
