from django.contrib.auth.base_user import BaseUserManager
from django.db.models import QuerySet

from utils.contants import UserTypes


class BaseUserQuerySet(QuerySet):
    """Custom queryset for models"""

    def hard_delete(self):
        super().delete()

    def delete(self):
        self.update(is_active=False)


class CustomUserManager(BaseUserManager.from_queryset(BaseUserQuerySet)):
    use_in_migrations = True

    def _create_user(self, phone_number, password=None, **extra_fields):
        # type: (str, str, dict) -> 'User'
        user = self.model(phone_number=phone_number, password=password, **extra_fields)
        if password:
            user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone_number, password=None, **extra_fields):
        # type: (str, str, dict) -> 'User'
        """
        to create customer
        """
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        user = self._create_user(
            phone_number=phone_number,
            password=password,
            user_type=UserTypes.CUSTOMER,
            **extra_fields
        )
        self._add_group(user, UserTypes.CUSTOMER)
        return user

    def create_superuser(self, phone_number, password=None, **extra_fields):
        # type: (str, str, dict) -> 'User'

        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        user = self._create_user(
            phone_number=phone_number,
            password=password,
            user_type=UserTypes.ADMIN,
            **extra_fields
        )
        self._add_group(user, UserTypes.ADMIN)
        return user

    def create_staff_user(
            self,
            phone_number,
            full_name,
            date_of_birth,
            gender,
            user_type,
            password=None,
            **extra_fields
    ):
        # type: (str, str, str, str, str, str, dict) -> 'User'
        """
        to create admin and waiter user
        """
        if user_type == UserTypes.ADMIN:
            extra_fields.setdefault("is_staff", True)

        user = self._create_user(
            phone_number=phone_number,
            full_name=full_name,
            date_of_birth=date_of_birth,
            gender=gender,
            password=password,
            user_type=user_type,
            **extra_fields
        )
        self._add_group(user, user_type)
        return user

    def _add_group(self, user, group_name):
        from django.contrib.auth.models import Group
        group, _ = Group.objects.get_or_create(name=group_name)
        user.groups.add(group)
        return user

    def _get_base_queryset(self):
        return super(CustomUserManager, self).get_queryset()

    def get_queryset(self):
        """
        Return NOT DELETED objects.
        """
        return self._get_base_queryset().filter(is_active=True)

    def deleted(self):
        """
        Return DELETED objects.
        """
        return self._get_base_queryset().filter(is_active=False)

    def with_deleted(self):
        """
        Return ALL objects.
        """
        return self._get_base_queryset()


class UserAuthManager:
    """
    For creating user, updating user data,
    sending confirmation code, etc.
    """

    def __init__(self, phone_number, **kwargs):
        self.phone_number = phone_number
        self.user = None

    def get_user(self, **kwargs):
        from user.models import User

        is_new = False
        try:
            user = User.objects.get(phone_number=self.phone_number)
        except User.DoesNotExist:
            is_new = True
            user = self._create_user(phone_number=self.phone_number, **kwargs)

        # every time user want to enter to app, we should send confirmation code
        # by confirming code, user can enter to app
        # after confirming code, we give user a token
        # if token is expired, user should confirm code again
        user.send_confirmation_code()

        return user, is_new

    @staticmethod
    def _create_user(phone_number, **kwargs):
        from user.models import User

        user = User.objects.create_user(phone_number=phone_number, **kwargs)
        return user


class UserUniqueIdentifierChecker:

    def __init__(self, phone_number):
        from user.models import User

        self.phone_number = phone_number
        self.user = User.objects.filter(phone_number=self.phone_number).first()

    def is_registered(self):
        if self.user and self.user.is_confirmed:
            return True
        return False

    def is_not_registered_yet(self):
        if not self.user:
            return True
        return False
