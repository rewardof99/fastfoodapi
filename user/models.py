from datetime import timedelta

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import NotAcceptable

from user.managers import CustomUserManager
from utils.services import send_confirmation_sms
from utils.validators import phone_number_validator

from utils.contants import (
    Languages,
    UserTypes,
    Gender
)

TOKEN_LENGTH = 6


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    1. for customer, full_name and phone_number are required. Other fields are optional.
    2. for admin and waiter full_name, phone_number, gender, date_of_birth
    """
    full_name = models.CharField(
        _("full name"),
        max_length=150,
    )
    phone_number = models.CharField(
        _('phone number'),
        validators=[phone_number_validator()],
        max_length=17,
        unique=True
    )
    language = models.CharField(
        verbose_name=_("preferred language"),
        choices=Languages.CHOICES,
        default=Languages.UZBEK,
        null=True,
    )
    date_of_birth = models.DateField(_("date of birth"), blank=True, null=True)
    gender = models.CharField(
        _("gender"), max_length=16,
        choices=Gender.CHOICES,
        null=True
    )
    user_type = models.CharField(
        _("user type"),
        max_length=10,
        choices=UserTypes.CHOICES,
        default=UserTypes.CUSTOMER,
    )
    address = models.TextField(_("address"), null=True)
    longitude = models.FloatField(
        _("longitude of user location"), null=True
    )
    latitude = models.FloatField(
        _("latitude of user location"), null=True
    )

    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_superuser = models.BooleanField(
        _("superuser status"),
        default=False,
        help_text=_(
            "Designates that this user has all permissions"
            " without explicitly assigning them."
        )
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    password = models.CharField(
        _("password"),
        max_length=128,
        null=True,
    )
    date_joined = models.DateTimeField(
        _("date joined"),
        default=timezone.now
    )
    # fields for confirmation of phone number
    confirmation_code = models.CharField(
        _("confirmation code"),
        max_length=6,
        null=True
    )
    sent_time = models.DateTimeField(_("sent time"), null=True)

    objects = CustomUserManager()
    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return self.full_name

    def generate_confirmation_code(self):
        """
        Returns a unique random `confirmation_code`
        Default token length = 6
        """
        token_length = TOKEN_LENGTH
        code = get_random_string(token_length, allowed_chars="0123456789")
        self.confirmation_code = code
        self.sent_time = timezone.now()
        self.save()

    def send_confirmation_code(self):
        if self.confirmation_code and self.sent_time and not self.is_confirmation_code_expired():
            raise NotAcceptable({"message": _("Confirmation code already sent, please use that one.")})
        self.generate_confirmation_code()
        send_confirmation_sms(self)

    def is_confirmation_code_expired(self):
        if self.confirmation_code and self.sent_time:
            expiration_date = self.sent_time + timedelta(minutes=settings.TOKEN_EXPIRE_MINUTES)
            return expiration_date <= timezone.now()
        return True

    def check_confirmation_code(self, confirmation_code: int):
        if not self.confirmation_code and not self.sent_time:
            raise NotAcceptable({"message": _("Confirmation code already used. Please get another one.")})
        if self.is_confirmation_code_expired():
            raise NotAcceptable({"message": _("Your Confirmation code is expired. Please get another one.")})
        if confirmation_code != self.confirmation_code:
            raise NotAcceptable({"message": _("Confirmation code did not match.")})

        self.confirmation_code = None
        self.sent_time = None
        self.save()

    def get_pair_token(self):
        from rest_framework_simplejwt.tokens import RefreshToken
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token)
        }

    def clear_cart(self):
        """
        Clears the cart of the user after the order is placed
        """
        from order.models import UserCart
        for item in UserCart.objects.filter(user=self):
            item.delete()
